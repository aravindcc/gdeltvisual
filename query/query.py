import gdelt
import geopandas as gpd
import pandas as pd
from dateutil import parser
from datetime import timedelta, datetime
from multiprocessing import Pool, cpu_count
import multiprocessing

# Version 2 queries
gd2 = gdelt.gdelt(version=2)


class NoDaemonProcess(multiprocessing.Process):
    # make 'daemon' attribute always return False
    @property
    def _get_daemon(self):  # pragma: no cover
        return False

    def _set_daemon(self, value):  # pragma: no cover
        pass

    daemon = property(_get_daemon, _set_daemon)


# We sub-class multiprocessing.pool.Pool instead of multiprocessing.Pool
# because the latter is only a wrapper function, not a proper class.
class NoDaemonProcessPool(multiprocessing.pool.Pool):
    Process = NoDaemonProcess


def nearest_15_time(date):
    date += timedelta(minutes=7, seconds=30)
    date -= timedelta(minutes=15 * round(date.minute/15),
                      seconds=date.second,
                      microseconds=date.microsecond)
    return date


def date_range(start, end, intv):
    start = nearest_15_time(parser.parse(start))
    end = nearest_15_time(parser.parse(end))
    current = start
    dates = []
    while (current + intv) < end:
        current += intv
        dates.append(current)
    dates.append(end)
    return convert_to_range(dates, intv)


def convert_to_range(dates, intv):
    if len(dates) == 0:
        return []

    if (intv == timedelta(minutes=15)):
        return list(map(lambda a: (None, a), dates))

    out = []
    for i in range(len(dates) - 1):
        nearest_end = nearest_15_time(dates[i + 1]) - timedelta(minutes=15)
        out.append((dates[i], nearest_end))
    return out


def process_date(date_range):
    (start, end) = date_range
    strftime = datetime.strftime
    print("STARTED ONE")
    result = None
    if start is None:
        result = gd2.Search(strftime(end, "%Y%m%d %H:%M"),
                            table='events', coverage=False, output='geodataframe')
    else:
        result = gd2.Search([
            strftime(start, "%Y %m %d %H:%M"),
            strftime(end, "%Y %m %d %H:%M"),
        ], table='events', coverage=True, output='geodataframe')
    print("FINISHED")
    return result


def start_guzzle():
    ranged = date_range("2019 11 02 10:00", "2019 11 03 05:00",
                        timedelta(hours=2))
    print(f"PROCESSING {len(ranged)}")
    results = []

    for rg in ranged:
        results.append(process_date(rg))
    results = pd.concat(results)

    # pool = NoDaemonProcessPool(processes=cpu_count())
    # downloaded_dfs = list(pool.imap_unordered(process_date, ranged))

    # pool.close()
    # pool.terminate()
    # pool.join()
    # # print(downloaded_dfs)
    # results = pd.concat(downloaded_dfs)
    # del downloaded_dfs

    country = "GBR"
    world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
    uk = world[world.iso_a3 == country]
    country_mask = (results.actor1countrycode == country) | (
        results.actor2countrycode == country) | (results.actiongeocountrycode == country)
    print(results.keys())
    print(results[country_mask].actiongeocountrycode)
    return results


def api_call(start, end, interval):
    strftime = datetime.strftime
    start = nearest_15_time(parser.parse(start))
    end = nearest_15_time(parser.parse(end))
    result = gd2.Search([
        strftime(start, "%Y %m %d %H:%M"),
        strftime(end, "%Y %m %d %H:%M"),
    ], table='events', coverage=True, output='geodataframe')
    print(result.keys())
    print(result.index)
    result = result.resample(interval)
    print(result)


api_call("2020 08 02 10:00", "2020 08 03 16:00",
         timedelta(hours=2))


# how are we caching this ??
# keep it simple cache the date queries
# save granularity, start date, count, [eventcodes], [goldsteinscores], [avgtone]


# get dates between range for granularity
# get each granularity in parralel
# check cache for each granularity
# get granularity
# split into countries
# get the required metric
# apply sliding window if required
# return line
