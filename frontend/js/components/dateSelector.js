import React, { useCallback, useEffect, useState } from 'react';
import { Button, DatePicker, Radio } from 'antd';
import { SyncOutlined } from '@ant-design/icons';

const { RangePicker } = DatePicker;

const optionEnums = {
  TIME: 'time',
  DAY: 'date',
  WEEK: 'week',
  MONTH: 'month',
  QUARTER: 'quarter',
  YEAR: 'year',
};

const options = Object.entries(optionEnums).map(([key, value]) => {
  return {
    label: key.charAt(0).toLocaleUpperCase() + key.toLocaleLowerCase().slice(1),
    value: value,
  };
});

const timeConfig = {
  format: 'HH:mm',
  disabledSeconds: () => false,
  hideDisabledOptions: true,
};

const getTimeFormat = (option) => {
  switch (option) {
    case optionEnums.TIME:
      return 'HH:mm DD-MM-YYYY';
    case optionEnums.DAY:
      return 'DD-MM-YYYY';
    case optionEnums.WEEK:
      return 'wo-YYYY';
    case optionEnums.MONTH:
      return 'MM-YYYY';
    case optionEnums.QUARTER:
      return 'QQ-YYYY';
    case optionEnums.YEAR:
      return 'YYYY';
  }
};

export default function DateSelector() {
  const [selectOption, setSelectedOption] = useState(optionEnums.DAY);
  const [params, setParams] = useState({});
  const [range, setRangeM] = useState();
  const [timeFormat, setTimeformat] = useState(getTimeFormat(optionEnums.DAY));

  const setRange = useCallback(
    (e) => {
      if (selectOption == optionEnums.TIME) {
        setRangeM(e);
      } else {
        console.log(
          [e[0].startOf(selectOption), e[1].endOf(selectOption)].map((m) =>
            m.format('HH:MM:SS DD-MM-YYYY')
          )
        );
        setRangeM([e[0].startOf(selectOption), e[1].endOf(selectOption)]);
      }
    },
    [setRangeM, selectOption]
  );
  const changeOption = useCallback((e) => setSelectedOption(e.target.value), [setSelectedOption]);

  useEffect(() => {
    if (selectOption == undefined) {
      setParams(undefined);
    } else if (selectOption == optionEnums.TIME) {
      setParams({
        showTime: timeConfig,
      });
    } else {
      setParams({ picker: selectOption });
    }
    setTimeformat(getTimeFormat(selectOption));
  }, [selectOption]);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <RangePicker
        style={{ marginRight: 25 }}
        {...params}
        value={range}
        onChange={setRange}
        format={timeFormat}
      />
      <Button type="primary" shape="circle" icon={<SyncOutlined />} style={{ marginRight: 75 }} />
      <Radio.Group
        options={options}
        onChange={changeOption}
        value={selectOption}
        optionType="button"
      />
    </div>
  );
}
