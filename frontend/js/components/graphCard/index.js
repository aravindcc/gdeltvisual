import { CaretUpOutlined, SyncOutlined } from '@ant-design/icons';
import React, { useState } from 'react';
import OptionSelect from '../optionSelect';
import { Line } from 'react-chartjs-2';
import PropTypes from 'prop-types';

import './style.scss';
import { Button, Empty, Skeleton } from 'antd';

const data = {
  labels: ['1', '2', '3', '4', '5', '6'],
  datasets: [
    {
      label: '# of Votes',
      data: [12, 19, 3, 5, 2, 3],
      fill: false,
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgba(255, 99, 132, 0.2)',
    },
  ],
};

const isEmpty = (data) => !data || Object.keys(data).length == 0;

function GraphCard({ isLoading }) {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div className="card">
      <div className="heading">
        <h2 className="text">Heading</h2>
        <CaretUpOutlined
          style={{
            transition: 'all .2s ease',
            transform: !isOpen ? 'rotate(180deg)' : null,
          }}
          onClick={() => setIsOpen(!isOpen)}
        />
        <Button type="primary" shape="circle" icon={<SyncOutlined />} />
      </div>

      <div
        className="options"
        style={{
          transition: 'all .2s ease',
          opacity: isOpen ? 1 : 0,
          height: !isOpen && 0,
          padding: !isOpen && 0,
        }}
      >
        <OptionSelect title={'Indicator Alert Countries'} placeholder={'add countries'} />
        <OptionSelect title={'Event Type'} placeholder={'add events'} />
      </div>

      <div className="graph">
        {isLoading ? (
          <Skeleton active />
        ) : isEmpty(data) ? (
          <Empty />
        ) : (
          <Line
            data={data}
            options={{
              zoomEnabled: true,
              panEnabled: true,
              title: {
                text: 'Try Zooming and Panning',
              },
              legend: {
                horizontalAlign: 'right',
                verticalAlign: 'center',
                position: 'bottom',
              },
              axisY: {
                includeZero: false,
              },
            }}
          />
        )}
      </div>
    </div>
  );
}

GraphCard.propTypes = {
  isLoading: PropTypes.bool,
};

GraphCard.defaultProps = {
  isLoading: true,
};

export default GraphCard;
