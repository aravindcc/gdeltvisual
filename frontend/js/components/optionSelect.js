import React, { useState } from 'react';

import { Select } from 'antd';

const OPTIONS = [...Array(60).keys()];

export default function OptionSelect({ title, placeholder }) {
  const [selectedItems, setSelectedItems] = useState([]);

  const filteredOptions = OPTIONS.filter((o) => !selectedItems.includes(o));
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <h5>{title}</h5>
      <Select
        mode="multiple"
        placeholder={placeholder}
        value={selectedItems}
        onChange={setSelectedItems}
        style={{ width: '100%' }}
      >
        {filteredOptions.map((item) => (
          <Select.Option key={item} value={item}>
            {item}
          </Select.Option>
        ))}
      </Select>
    </div>
  );
}
