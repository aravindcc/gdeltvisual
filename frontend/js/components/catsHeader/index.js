import React, { createRef, useEffect } from 'react';
import DateSelector from '../dateSelector';

import './style.scss';

function onScrollEventHandler() {
  var el = document.getElementsByClassName('catsheader')[0];
  var doc = document.documentElement;
  var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

  if (top > 0) {
    el.classList.add('stickied');
  } else {
    el.classList.remove('stickied');
  }
}

export default function CATSHeader() {
  const headerRef = createRef();

  useEffect(() => {
    window.addEventListener('scroll', onScrollEventHandler, false);
    return () => window.removeEventListener('scroll', onScrollEventHandler);
  }, []);

  return (
    <div className={'catsheader'} ref={headerRef}>
      <h1 style={{ textAlign: 'center' }}>Context Alert and Trend System (CATS)</h1>
      <DateSelector />
    </div>
  );
}
