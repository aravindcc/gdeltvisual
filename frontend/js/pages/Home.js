import { Space } from 'antd';
import React from 'react';
import GraphCard from '../components/graphCard';
import CATSHeader from '../components/catsHeader';

const Home = () => {
  return (
    <>
      <CATSHeader />
      <Space className="graphContainer" direction="vertical" style={{ width: '100%', padding: 30 }}>
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
        <GraphCard isLoading={false} />
      </Space>
    </>
  );
};

export default Home;
