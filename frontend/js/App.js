import React from 'react';
import { hot } from 'react-hot-loader/root';

import 'antd/dist/antd.css';

import Home from './pages/Home';

const App = () => <Home />;

export default hot(App);
