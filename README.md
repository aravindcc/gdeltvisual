[![License: MIT](https://img.shields.io/github/license/vintasoftware/django-react-boilerplate.svg)](LICENSE.txt)

# Gdelt Visual

## About

This was made using the vintasoftware/django-react-boilerplate template.

## Running

### Setup

- Create the migrations for `users` app:  
  `docker-compose run --rm backend python manage.py makemigrations`
- Run the migrations:
  `docker-compose run --rm backend python manage.py migrate`

- Open a command line window and go to the project's directory.
- `docker-compose up -d`
  To access the logs for each service run `docker-compose logs -f service_name` (either backend, frontend, etc)

### Adding packages

- To install a new npm package you can create a bash session inside the running `frontend` container.
- To install a new PyPi package you will have to update the `requirements.in` file and rebuild the `backend` container.

### Notes

- So far the React based frontend has been built out.
- I am still unsure about how we are querying GDELT so the query folder is being used to experiment the base query.
- To run `query.py` create a bash session in the `backend` container. Run `cd ../query/` to navigate to query folder. Then `python query.py` to run the python file.
- This query will then need to be served up through an API on the backend (however this will mainly be boilerplate marshalling code).
- The plan is use do the djangorestframework to this adding https://www.django-rest-framework.org/api-guide/throttling/, to make this a viable public api.
- If the plan is to use BigQuery there should be no need for a backend database, since simple moving average queries can be cached on there.
- Otherwise like a backend to cache results will be useful.
